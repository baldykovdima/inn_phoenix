let InnChannel = {
  init(socket) {
    let channel = socket.channel("inn_channel:lobby", {});
    channel.join();
    this.listenForList(channel);
  },

  listenForList(channel) {
    document.getElementById("inn-form").addEventListener("submit", function(e) {
      e.preventDefault();

      let inn = document.getElementById("input-value").value;
      channel.push("shout", { inn }).receive("locked", resp => {
        alert("Мы вычислили вас по айпи")
      });

      document.getElementById("input-value").value = "";
    });

    channel.on("shout", payload => {
      let list = document.querySelector("#list");
      let line = document.createElement("p");
      console.log(payload);
      line.insertAdjacentHTML(
        "beforeend",
        `[${new Date(payload.checkDate).toLocaleString()}] ${payload.inn} : ${
          payload.checkResult ? "корректен" : "некорректен"
        }`
      );
      list.prepend(line);
    });
  }
};

export default InnChannel;
