defmodule InnChecker.Repo.Migrations.CreateChecks do
  use Ecto.Migration

  def change do
    create table(:checks) do
      add :checkDate, :naive_datetime
      add :inn, :string
      add :checkResult, :boolean, default: false, null: false
      add :ip, :string

      timestamps()
    end

  end
end
