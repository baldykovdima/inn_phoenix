defmodule InnChecker.InnTest do
  use InnChecker.DataCase

  alias InnChecker.Inn

  describe "checks" do
    alias InnChecker.Inn.Check

    @valid_attrs %{checkDate: ~N[2010-04-17 14:00:00], checkResult: true, inn: "some inn", ip: "some ip"}
    @update_attrs %{checkDate: ~N[2011-05-18 15:01:01], checkResult: false, inn: "some updated inn", ip: "some updated ip"}
    @invalid_attrs %{checkDate: nil, checkResult: nil, inn: nil, ip: nil}

    def check_fixture(attrs \\ %{}) do
      {:ok, check} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Inn.create_check()

      check
    end

    test "list_checks/0 returns all checks" do
      check = check_fixture()
      assert Inn.list_checks() == [check]
    end

    test "get_check!/1 returns the check with given id" do
      check = check_fixture()
      assert Inn.get_check!(check.id) == check
    end

    test "create_check/1 with valid data creates a check" do
      assert {:ok, %Check{} = check} = Inn.create_check(@valid_attrs)
      assert check.checkDate == ~N[2010-04-17 14:00:00]
      assert check.checkResult == true
      assert check.inn == "some inn"
      assert check.ip == "some ip"
    end

    test "create_check/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Inn.create_check(@invalid_attrs)
    end

    test "update_check/2 with valid data updates the check" do
      check = check_fixture()
      assert {:ok, %Check{} = check} = Inn.update_check(check, @update_attrs)
      assert check.checkDate == ~N[2011-05-18 15:01:01]
      assert check.checkResult == false
      assert check.inn == "some updated inn"
      assert check.ip == "some updated ip"
    end

    test "update_check/2 with invalid data returns error changeset" do
      check = check_fixture()
      assert {:error, %Ecto.Changeset{}} = Inn.update_check(check, @invalid_attrs)
      assert check == Inn.get_check!(check.id)
    end

    test "delete_check/1 deletes the check" do
      check = check_fixture()
      assert {:ok, %Check{}} = Inn.delete_check(check)
      assert_raise Ecto.NoResultsError, fn -> Inn.get_check!(check.id) end
    end

    test "change_check/1 returns a check changeset" do
      check = check_fixture()
      assert %Ecto.Changeset{} = Inn.change_check(check)
    end
  end
end
