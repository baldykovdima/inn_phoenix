defmodule InnCheckerWeb.VerifyTest do
    use ExUnit.Case

    alias InnChecker.Verify
  
    test "check/1 correct" do
      result = Verify.check("183106832840")
      assert result == :ok
    end

    test "check/1 incorrect" do
        result = Verify.check("123456789000")
        assert result == {:error, :incorrect}
    end

    test "check/1 length" do
        result = Verify.check("123")
        assert result == {:error, :length}
    end

    test "check/1 format" do
        result = Verify.check("123fsad3423")
        assert result == {:error, :format}
    end
  end
  