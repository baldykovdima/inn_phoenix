defmodule InnCheckerWeb.InnChannelChannelTest do
  use InnCheckerWeb.ChannelCase

  setup do
    {:ok, _, socket} =
      socket(InnCheckerWeb.UserSocket, "user_id", %{some: :assign})
      |> subscribe_and_join(InnCheckerWeb.InnChannelChannel, "inn_channel:lobby")

    {:ok, socket: socket}
  end

  test "shout broadcasts to inn_channel:lobby", %{socket: socket} do
    push socket, "shout", %{"inn" => "123", "ip" => "127.0.0.1"}
    assert_broadcast "shout", %{"inn" => "123", "ip" => "127.0.0.1"}
  end

end
