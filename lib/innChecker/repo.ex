defmodule InnChecker.Repo do
  use Ecto.Repo,
    otp_app: :innChecker,
    adapter: Ecto.Adapters.Postgres
end
