defmodule InnChecker.Verify do
  require Logger

  @multipliers10 [2, 4, 10, 3, 5, 9, 4, 6, 8]
  @multipliers12 [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]
  @multipliers12_1 [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]

  @doc """
  Проверяет контрольную сумму ИНН, допускаются значения из 10 и 12 чисел в формате ХХХХХХХХХХ и ХХХХХХХХХХХХ


  ## Примеры

      iex> Verify.check(183106832840)
      :ok

      iex> Verify.check(1234567890)
      {:error, :incorrect}

      iex> Verify.check(123)
      {:error, :length}

      iex> Verify.check(ffffff)
      {:error, :format}

  """
  @spec check(String.t()) :: :ok | {:error, :format | :incorrect | :length}
  def check(inn) do
    Logger.debug("Check INN #{inn}")

    with :ok <- check_format(inn) do
      case check_correct(inn) do
        :ok ->
          Logger.debug("INN #{inn} correct")
          :ok

        {:error, type} ->
          Logger.debug("INN #{inn} incorrect #{type}")
          {:error, type}
      end
    end
  end

  defp check_correct(inn) do
    case String.length(inn) do
      10 -> check_10(inn)
      12 -> check_12(inn)
      _ -> {:error, :length}
    end
  end

  defp check_10(inn) do
    control_number = control_number(@multipliers10, inn)
    Logger.debug("INN #{inn} check_10 control_number #{control_number}")

    case String.at(inn, 9) == control_number do
      true -> :ok
      _ -> {:error, :incorrect}
    end
  end

  defp check_12(inn) do
    control_number_first = control_number(@multipliers12, inn)
    Logger.debug("INN #{inn} first control number #{control_number_first}")

    case String.at(inn, 10) == control_number_first do
      true ->
        control_number_second = control_number(@multipliers12_1, inn)
        Logger.debug("INN #{inn} second control number #{control_number_second}")

        case String.at(inn, 11) == control_number_second do
          true -> :ok
          _ -> {:error, :incorrect}
        end

      _ ->
        {:error, :incorrect}
    end
  end

  defp control_number(multipliers, inn) do
    multipliers
    |> Enum.with_index()
    |> Enum.map(fn {item, index} -> (String.at(inn, index) |> String.to_integer()) * item end)
    |> Enum.sum()
    |> control_number_from_check_sum()
    |> Integer.to_string()
  end

  defp control_number_from_check_sum(check_sum) do
    control_number = check_sum - div(check_sum, 11) * 11

    case control_number do
      10 -> 0
      _ -> control_number
    end
  end

  defp check_format(inn) do
    case Regex.match?(~r/^\d+$/, inn) do
      true ->
        :ok

      false ->
        Logger.debug("INN #{inn} incorrect fromat")
        {:error, :format}
    end
  end
end
