defmodule InnChecker.IpLock do
  def is_locked(list, item) do
    {:ok, conn} = Redix.start_link("redis://localhost:6379/3")
    {:ok, rank} = Redix.command(conn, ["ZRANK", list, item])

    case rank do
      nil -> :ok
      _ -> :locked
    end
  end
end
