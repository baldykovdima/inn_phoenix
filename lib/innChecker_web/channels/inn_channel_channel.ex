defmodule InnCheckerWeb.InnChannelChannel do
  use InnCheckerWeb, :channel

  alias InnChecker.Inn
  alias InnChecker.Verify
  alias InnChecker.IpLock

  def join("inn_channel:lobby", _payload, socket) do
    list = Inn.list_by_date_desc() |> Jason.encode!()
    {:ok, %{list: list}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (inn_channel:lobby).
  def handle_in("shout", payload, socket) do
    case IpLock.is_locked("locks", socket.assigns.ip) do
      :locked ->
        {:reply, :locked, socket}

      _ ->
        check_result = payload["inn"] |> Verify.check()

        check =
          payload
          |> Map.put_new("ip", socket.assigns.ip)
          |> Map.put_new("checkResult", check_result == :ok)
          |> Map.put_new("checkDate", DateTime.utc_now())

        Inn.create_check(check)

        broadcast(socket, "shout", check)
        {:noreply, socket}
    end
  end
end
