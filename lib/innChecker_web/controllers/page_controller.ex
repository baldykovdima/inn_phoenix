defmodule InnCheckerWeb.PageController do
  use InnCheckerWeb, :controller

  alias InnChecker.Inn

  def index(conn, _params) do
    list = Inn.list_by_date_desc()

    render(conn, "index.html", list: list, ip: conn.remote_ip |> :inet_parse.ntoa() |> to_string())
  end
end
